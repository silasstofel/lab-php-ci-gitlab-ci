<?php


class ExampleTest extends \Codeception\Test\Unit
{

  private $user;
  /**
   * @var \UnitTester
   */
  protected $tester;

  protected function _before()
  {
    $this->user = [
      'id' => 1, 'name' => 'Silas Stoffel', 'active' => true
    ];
  }

  protected function _after()
  {
  }

  // tests
  public function testValidation()
  {
    $this->assertTrue($this->user['active']);
    $this->assertEquals($this->user['id'], 1);
    $this->assertEquals($this->user['name'], 'Silas Stoffel');
  }
}
