<?php

global $_config;
$db = $_config['database'];
$dsn = sprintf($db['dsn'], $db['server'], $db['database']);

try {
  R::setup($dsn, $db['user'], $db['pass']);
  R::freeze(true);
} catch (Exception $exc) {
  throw $exc;
}
